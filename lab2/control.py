import datetime
import db_storage as dbs


def validate(date_text):
    try:
        datetime.datetime.strptime(date_text, '%Y-%m-%d')
    except ValueError:
        print("Incorrect data format, should be YYYY-MM-DD")
        return False
    return True


def add_student():
    print("Enter student name")
    name = input("Enter key name:")
    print("Enter student age")
    age = eval(input("Enter student age:"))
    while age > 18 or age < 10:
        print("Age must be under 18 and bigger than 10")
        age = eval(input("Enter student age:"))
    dbs.add_student(name, age)


def rm_student():
    print("Enter student id")
    df = dbs.get_students()
    ids = df['st_id']
    st_id = eval(input("Enter student id:"))
    while st_id not in ids:
        print("Such id not exist")
        st_id = eval(input("Enter student id:"))
    dbs.rm_student(st_id)


def upd_student():
    print("Enter student id")
    st_id = eval(input("Enter student id:"))
    df = dbs.get_student_by_id(st_id)
    ids = df['st_id']
    while st_id not in ids:
        print("Such id not exist")
        st_id = eval(input("Enter student id:"))
    print(df)
    print("Enter new name:")
    name = input("Enter key name:")
    print("Enter student age")
    age = eval(input("Enter student age:"))
    while age > 18 or age < 10:
        print("Age must be under 18 and bigger than 10")
        age = eval(input("Enter student age:"))
    dbs.upd_student(st_id, name, age)


def add_teacher():
    print("Teacher added")


def rm_teacher():
    print("Teacher removed")


def upd_teacher():
    print("Teacher updated")


def add_class():
    print("Class added")


def rm_class():
    print("Class removed")


def upd_class():
    print("Class updated")


def find_subj_for_class_by_dur():
    k = eval(input("Enter 1 to run def"))
    if k == 1:
        print(dbs.find_subj_for_class_by_dur(932, 167, 10, 50))
        return
    print("Enter class id")
    df = dbs.get_class()
    class_ids = df['class_id']
    class_id = eval(input("Enter class id:"))
    while class_id not in class_ids:
        print("No such class id")
        class_id = eval(input("Enter class id:"))
    df = dbs.get_teacher_list()
    tch_ids = df['tch_id']
    tch_id = eval(input("Enter teacher id:"))
    while tch_id not in tch_ids:
        print("No such teacher id")
        tch_id = eval(input("Enter teacher id:"))
    duration_range_1 = eval(input("Enter duration range min:"))
    while duration_range_1 < 10 or duration_range_1 > 50:
        print("Wrong range")
        duration_range_1 = eval(input("Enter duration range min:"))
    duration_range_2 = eval(input("Enter duration range max:"))
    while duration_range_1 > duration_range_2 or duration_range_2 > 50:
        print("Wrong range")
        duration_range_2 = eval(input("Enter duration range max:"))
    print(dbs.find_subj_for_class_by_dur(class_id, tch_id, duration_range_1, duration_range_2))


def teacher_teach_type_for():
    k = eval(input("Enter 1 to run def"))
    if k == 1:
        print(dbs.teacher_teach_type_for(167, 50, 30, 60))
        return
    df = dbs.get_teacher_list()
    tch_ids = df['tch_id']
    tch_id = eval(input("Enter teacher id:"))
    while tch_id not in tch_ids:
        print("No such teacher id")
        tch_id = eval(input("Enter teacher id:"))
    df = dbs.get_types()
    t_ids = df['t_id']
    t_id = eval(input("Enter type id:"))
    while t_id not in t_ids:
        print("No such type id")
        tch_id = eval(input("Enter type id:"))
    duration_range_1 = eval(input("Enter duration range min:"))
    while duration_range_1 < 10 or duration_range_1 > 50:
        print("Wrong range")
        duration_range_1 = eval(input("Enter duration range min:"))
    duration_range_2 = eval(input("Enter duration range max:"))
    while duration_range_1 > duration_range_2 or duration_range_2 > 50:
        print("Wrong range")
        duration_range_2 = eval(input("Enter duration range max:"))
    print(dbs.teacher_teach_type_for(tch_id, t_id, duration_range_1, duration_range_2))


def teacher_meet_student():
    k = eval(input("Enter 1 to run def"))
    if k == 1:
        print(dbs.student_meet_teacher(150, 1256))
        return
    df = dbs.get_teacher_list()
    tch_ids = df['tch_id']
    tch_id = eval(input("Enter teacher id:"))
    while tch_id not in tch_ids:
        print("No such teacher id")
        tch_id = eval(input("Enter teacher id:"))
    df = dbs.get_students()
    ids = df['st_id']
    st_id = eval(input("Enter student id:"))
    while st_id not in ids:
        print("Such id not exist")
        st_id = eval(input("Enter student id:"))
    print(dbs.student_meet_teacher(st_id, tch_id))


def teacher_menu():
    while True:
        print("1 - see teachers")
        print("2 - add teacher")
        print("3 - delete teacher")
        print("4 - update teacher")
        print("5 - exit")
        k = eval(input("Enter key:"))
        if k == 1:
            print(dbs.get_teacher_list())
        elif k == 2:
            add_teacher()
        elif k == 3:
            rm_teacher()
        elif k == 4:
            upd_teacher()
        elif k == 5:
            break
        else:
            print("Enter right key")


def student_menu():
    while True:
        print("1 - see students")
        print("2 - add students")
        print("3 - delete students")
        print("4 - update students")
        print("5 - exit")
        k = eval(input("Enter key:"))
        if k == 1:
            print(dbs.get_students())
        elif k == 2:
            add_student()
        elif k == 3:
            rm_student()
        elif k == 4:
            upd_student()
        elif k == 5:
            break
        else:
            print("Enter right key")


def classes_menu():
    while True:
        print("1 - see classes")
        print("2 - add classes")
        print("3 - delete classes")
        print("4 - update classes")
        print("5 - exit")
        k = eval(input("Enter key:"))
        if k == 1:
            print(dbs.get_class())
        elif k == 2:
            add_class()
        elif k == 3:
            rm_class()
        elif k == 4:
            upd_class()
        elif k == 5:
            break
        else:
            print("Enter right key")


def table_menu():
    while True:
        print("1 - see teacher")
        print("2 - see students")
        print("3 - see classes")
        print("4 - see subjects")
        print("5 - see types")
        print("6 - see schedule")
        print("7 - exit")
        k = eval(input("Enter key:"))
        if k == 1:
            teacher_menu()
        elif k == 2:
            student_menu()
        elif k == 3:
            classes_menu()
        elif k == 4:
            print(dbs.get_subjects())
        elif k == 5:
            print(dbs.get_types())
        elif k == 6:
            print(dbs.get_schedule())
        elif k == 7:
            break
        else:
            print("No such key")


def search_menu():
    while True:
        print("Search menu:")
        print("1 - when teacher A meet student B")
        print("2 - when teacher A teach type B")
        print("3 - when class A learn B for n time")
        print("4 - exit")
        k = eval(input("Enter key"))
        if k == 1:
            teacher_meet_student()
        elif k == 2:
            teacher_teach_type_for()
        elif k == 3:
            find_subj_for_class_by_dur()
        elif k == 4:
            break
        else:
            print("Enter right key")
