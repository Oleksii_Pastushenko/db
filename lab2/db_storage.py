import psycopg2
import pandas as pd
import random as rd
import time


def get_types():
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="1111",
        port="5432")
    sql = "select * from lab1.types"
    df = pd.read_sql(sql, connection)
    connection.close()
    return df


def get_teacher_list():
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="1111",
        port="5432")
    sql = "select * from lab1.teachers"
    df = pd.read_sql(sql, connection)
    connection.close()
    return df


def get_schedule():
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="1111",
        port="5432")
    sql = "select * from lab1.schedule"
    df = pd.read_sql(sql, connection)
    connection.close()
    return df


def get_class():
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="1111",
        port="5432")
    sql = "select * from lab1.class"
    df = pd.read_sql(sql, connection)
    connection.close()
    return df


def get_students():
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="1111",
        port="5432")
    sql = "select * from lab1.students"
    df = pd.read_sql(sql, connection)
    connection.close()
    return df


def get_subjects():
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="1111",
        port="5432")
    sql = "select * from lab1.subjects"
    df = pd.read_sql(sql, connection)
    connection.close()
    return df


def get_class_schedule(class_id):
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="1111",
        port="5432")
    sql_scheme = "select * from lab1.schedule where class_id = {}"
    sql = sql_scheme.format(class_id)
    df = pd.read_sql(sql, connection)
    connection.close()
    return df


def create_teachers():
    sql = "insert into lab1.teachers (fullname , expirience) " \
          "values (chr(trunc(65+random()*25)::int) " \
          "|| chr(trunc(65+random()*25)::int)," \
          "trunc(1 + random()*20)::int);"
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="1111",
        port="5432")
    cursor = connection.cursor()
    for i in range(1000):
        cursor.execute(sql)
    connection.commit()
    cursor.close()
    connection.close()


def create_classes():
    sql = "insert into lab1.class (name) values (chr(trunc(48+random()*4)::int) " \
          "|| chr(trunc(65+random()*5)::int));"
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="1111",
        port="5432")
    cursor = connection.cursor()
    for i in range(1000):
        cursor.execute(sql)
    connection.commit()
    cursor.close()
    connection.close()


def create_students():
    sql = "insert into lab1.students (fullname , age) " \
          "values (chr(trunc(65+random()*25)::int) " \
          "|| chr(trunc(65+random()*25)::int)," \
          "trunc(10 + random()*6)::int);"
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="1111",
        port="5432")
    cursor = connection.cursor()
    for i in range(1000):
        cursor.execute(sql)
    connection.commit()
    cursor.close()
    connection.close()


def create_types():
    sql = "insert into lab1.types (t_name) " \
          "values (chr(trunc(65+random()*25)::int) " \
          "|| chr(trunc(65+random()*25)::int));"
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="1111",
        port="5432")
    cursor = connection.cursor()
    for i in range(1000):
        cursor.execute(sql)
    connection.commit()
    cursor.close()
    connection.close()


def create_subjects():
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="1111",
        port="5432")
    df = pd.read_sql('select t_id from lab1.types', connection)
    t_id = df['t_id']
    sql_scheme = "insert into lab1.subjects (t_id,sb_name) values ({},chr(trunc(65+random()*25)::int) " \
                 "|| chr(trunc(65+random()*25)::int));"
    cursor = connection.cursor()
    for i in range(1000):
        sql = sql_scheme.format(rd.choice(t_id))
        cursor.execute(sql)
    connection.commit()
    cursor.close()
    connection.close()


def place_students_in_classes():
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="1111",
        port="5432")
    df = pd.read_sql('select st_id from lab1.students where st_id not in (select st_id from lab1.st_class)', connection)
    st_ids = df['st_id']
    df = pd.read_sql('select class_id from lab1.class', connection)
    class_id = df['class_id']
    sql_scheme = "insert into lab1.st_class (class_id,st_id) values ({},{})"
    cursor = connection.cursor()
    for i in st_ids:
        sql = sql_scheme.format(rd.choice(class_id), i)
        cursor.execute(sql)
    connection.commit()
    cursor.close()
    connection.close()


def connect_teacher_to_subject():
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="1111",
        port="5432")
    df = pd.read_sql('select sb_id from lab1.subjects', connection)
    sb_ids = df['sb_id']
    df = pd.read_sql('select tch_id from lab1.teachers', connection)
    tch__id = df['tch_id']
    sql_scheme = "insert into lab1.sb_tch (tch_id,sb_id) values ({},{})"
    cursor = connection.cursor()
    for i in range(1000):
        sql = sql_scheme.format(rd.choice(tch__id), rd.choice(sb_ids))
        cursor.execute(sql)
    connection.commit()
    cursor.close()
    connection.close()


def create_schedule():
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="1111",
        port="5432")
    df = pd.read_sql('select sb_id from lab1.subjects', connection)
    sb_ids = df['sb_id']
    df = pd.read_sql('select class_id from lab1.class', connection)
    class_id = df['class_id']
    sql_scheme = "insert into lab1.schedule (class_id,sb_id,date,duration) values ({},{}," \
                 "(timestamp '2020-10-10 20:00:00' +random() * (timestamp '2020-10-20 20:00:00' " \
                 "-timestamp '2020-10-30 10:00:00'))::date, trunc(15+random()*30)::int)"
    cursor = connection.cursor()
    for i in range(1000):
        sql = sql_scheme.format(rd.choice(class_id), rd.choice(sb_ids))
        cursor.execute(sql)
    connection.commit()
    cursor.close()
    connection.close()


def get_student_by_id(st_id):
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="1111",
        port="5432")
    sql = "select * from lab1.students where st_id = {}"
    df = pd.read_sql(sql.format(st_id), connection)
    connection.close()
    return df


def add_student(name, age):
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="1111",
        port="5432")
    cursor = connection.cursor()
    sql_scheme = "insert into lab1.students (fullname, age) values ('{}',{})"
    cursor.execute(sql_scheme.format(name, age))
    connection.commit()
    cursor.close()
    connection.close()


def rm_student(st_id):
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="1111",
        port="5432")
    cursor = connection.cursor()
    sql_scheme = "delete from lab1.st_class where st_id ={};" \
                 "delete from lab1.students where st_id = {};"
    cursor.execute(sql_scheme.format(st_id, st_id))
    connection.commit()
    cursor.close()
    connection.close()


def upd_student(st_id, name, age):
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="1111",
        port="5432")
    cursor = connection.cursor()
    sql_scheme = "update lab1.students set (fullname, age) = ('{}', {}) where st_id = {};"
    cursor.execute(sql_scheme.format(name, age, st_id))
    connection.commit()
    cursor.close()
    connection.close()


def add_teacher():
    print()


def rm_teacher():
    print()


def upd_teacher():
    print()


def add_class():
    print()


def rm_class():
    print()


def upd_class():
    print()


def find_subj_for_class_by_dur(class_id, tch_id, start, end):
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="1111",
        port="5432")
    sql_scheme = "select * from lab1.teachers t inner join (select * from lab1.sb_tch) sb on sb.tch_id = t.tch_id " \
                 "where sb_id in (select sb_id from lab1.schedule where duration > {} " \
                 "and duration < {} and class_id = {})" \
                 "and t.tch_id = {}"
    start_time = time.time()
    df = pd.read_sql(sql_scheme.format(start, end, class_id, tch_id), connection)
    end_time = time.time()
    query_time = "Query time:"
    query_time_s = (end_time - start_time) * 1000
    print(query_time.format(query_time_s))
    connection.close()
    return df


def student_meet_teacher(st_id, tch_id):
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="1111",
        port="5432")
    sql_scheme = "with classes as(" \
                 "select * from lab1.students st " \
                 "inner join (select * from lab1.st_class) sc on sc.st_id = st.st_id where st.st_id = {} ) ," \
                 "sb_on_tch as(" \
                 "select * from lab1.teachers t " \
                 "inner join (select * from lab1.sb_tch) sb on sb.tch_id = t.tch_id where t.tch_id = {})," \
                 "cs_on_sb as(" \
                 "select * from classes clst " \
                 "inner join (select * from lab1.schedule) sch on sch.class_id = clst.class_id)" \
                 "select cb.fullname as tch_fullname, sh.fullname as st_fullname, date, ls_id " \
                 "from sb_on_tch cb inner join (select * from cs_on_sb) sh on sh.sb_id = cb.sb_id"
    start_time = time.time()
    df = pd.read_sql(sql_scheme.format(st_id, tch_id), connection)
    end_time = time.time()
    query_time = "Query time in ms:"
    query_time_s = (end_time - start_time) * 1000
    print(query_time, query_time_s)
    connection.close()
    return df


def teacher_teach_type_for(tch_id, t_id, min_t, max_t):
    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="1111",
        port="5432")
    sql_scheme = "with typ as (" \
                 " select * from lab1.types ty  " \
                 "inner join (select * from lab1.subjects) sb on sb.t_id = ty.t_id where ty.t_id = {})," \
                 " sb_on_tch as( " \
                 "select * from lab1.teachers t " \
                 "inner join (select * from lab1.sb_tch) sb on sb.tch_id = t.tch_id where t.tch_id = {}) " \
                 "select ty.t_name, fullname, date, duration " \
                 "from typ ty inner join (select * from sb_on_tch) sbt on sbt.sb_id = ty.sb_id " \
                 "inner join (select * from lab1.schedule) sch on sch.sb_id = ty.sb_id " \
                 "where duration > {} and duration < {}"
    start_time = time.time()
    df = pd.read_sql(sql_scheme.format(t_id, tch_id, min_t, max_t), connection)
    end_time = time.time()
    query_time = "Query time in ms:"
    query_time_s = (end_time - start_time) * 1000
    print(query_time, query_time_s)
    connection.close()
    return df
